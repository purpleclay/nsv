# NSV

NSV (Next Semantic Version) is a convention-based semantic versioning tool that leans on the power of conventional commits to make versioning your software a breeze!

## Why another versioning tool

There are many semantic versioning tools already out there! But they typically require some configuration or custom scripting in your CI system to make them work. No one likes managing config; it is error-prone, and the slightest tweak ultimately triggers a cascade of change across your projects.

Step in NSV. Designed to make intelligent semantic versioning decisions about your project without needing a config file. Entirely convention-based, you can adapt your workflow from within your commit message.

The power is at your fingertips.

### Features

- First-class support for semantic versioning. Conventional commits help give it a nudge in the right direction.
- If you batch your commits per release or prefer a continuous delivery approach, it has you covered.
- Context-aware, it automatically switches to a monorepo workflow.
- Extend the power of your commits through commands to dynamically change your semantic release workflow.
- Explore how to use it within the purpose-built playground.

## Documentation

Check out the latest [documentation](https://docs.purpleclay.dev/nsv/)

## Importing the job template

It is quick and easy to add `nsv` to your pipeline. Just include the `nsv.gitlab-ci.yml` template directly within your `.gitlab-ci.yml` file:

```yaml
include:
  - https://gitlab.com/purpleclay/nsv/-/raw/main/nsv.gitlab-ci.yml
```

This template runs `nsv` as a stageless job as part of a DAG (Directed Acyclic Graph) pipeline. You can change this behavior by overriding the job template and assigning it to a stage:

```yaml
include:
  - https://gitlab.com/purpleclay/nsv/-/raw/main/nsv.gitlab-ci.yml

stages:
  - release

nsv:
  stage: release
```

The `nsv` job will output the next semantic version to the `NSV` environment variable for use within other jobs or stages.

## Job environment variables

| Name                | Required       | Description                                                                              |
| ------------------- | -------------- | ---------------------------------------------------------------------------------------- |
| `NSV_IMAGE_VERSION` | Yes            | The tag name of the latest published version of `nsv` (default = `v0.3.0`)               |
| `NSV_GITLAB_TOKEN`  | No<sup>1</sup> | A GitLab access token required to push a tag back to the remote.                         |
| `NSV_NEXT_ONLY`     | No             | If set, the next semantic version will be calculated without the repository being tagged |

<sub>1: Only takes effect if the `NSV_NEXT_ONLY` environment variable is not set.</sub>

You can also define CI/CD variables within your GitLab project to configure the behavior of both [nsv](https://docs.purpleclay.dev/nsv/reference/env-vars/) and [gpg-import](https://github.com/purpleclay/gpg-import/blob/main/README.md#features).

| Name              | Required | Description                                                     |
| ----------------- | -------- | --------------------------------------------------------------- |
| `NSV_FORMAT`      | No       | A Go template for formatting the generated semantic version tag |
| `NSV_TAG_MESSAGE` | No       | A custom message when creating an annotated tag                 |
| `GPG_PRIVATE_KEY` | No       | The base64 encoded GPG private key in armor format              |
| `GPG_PASSPHRASE`  | No       | An optional passphrase if the GPG key is password protected.    |
| `GPG_TRUST_LEVEL` | No       | An owner trust level assigned to the imported GPG key           |

## GPG tag signing

`nsv` supports the GPG signing of tags by detecting the optional `GPG_PRIVATE_KEY` environment variable. If present, [gpg-import](https://github.com/purpleclay/gpg-import) will import the key and configure git accordingly.

Any [generated](https://docs.github.com/en/authentication/managing-commit-signature-verification/generating-a-new-gpg-key) GPG key must be exported as a `base64` encoded ASCII armored string:

```sh
# macos
gpg --armor --export-secret-key batman@dc.com | base64 | pbcopy

# linux
gpg --armor --export-secret-key batman@dc.com | base64 | xclip
```

## Committer details

When tagging your repository, `nsv` will identify the person associated with the commit that triggered the release and pass this to git through the `user.name` and `user.email` config settings.

You can override this behavior by importing a GPG key, manually setting those git config settings, or using the reserved git environment variables `GIT_COMMITTER_NAME` and `GIT_COMMITTER_EMAIL`; see the [documentation](https://docs.purpleclay.dev/nsv/tag-version/#committer-impersonation) for further details.

## GitLab access token

To ensure `nsv` can push changes back to your GitLab project, you must provide it with an access token<sup>1</sup> containing the `write_repository` permission. This access token must be made available through the `NSV_GITLAB_TOKEN` environment variable.

Without it, the `nsv` job will fail with the following error:

```sh
2023/06/29 04:52:41 failed to execute git command: git push origin '0.1.0'
remote: You are not allowed to upload code.
fatal: unable to access 'https://gitlab.com/purpleclay/test.git/': The requested URL returned error: 403
```

<sub>1: if using gitlab.com, please ensure the access token belongs to a verified user to prevent any tag pipeline from failing.</sub>
